package is.kow.finatratypesafe.services

import java.io.File
import javax.inject.{Inject, Singleton}

import com.twitter.finatra.annotations.Flag
import com.twitter.inject.Logging

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class RepoMirroringService @Inject()(@Flag("repo.base") repoBase: String) extends Logging {

  import ExecutionContext.Implicits.global

  /**
   *     "url": "git@example.com:mike/diasporadiaspora.git",
   * @param remoteUrl
   */
  def updateOrCloneRepo(remoteUrl: String): Unit = {
    val localPath = repoBase + "/" + remoteUrl.replaceAll("git@gitlab.com:", "")
    logger.info(s"mirroring for $localPath")

    //Explicitly using the Scala Futures instead of the twitter futures
    Future {
      //If the repo exists, fetch it, if that fails clone it
      import scala.sys.process._

      //Assuming git is on the path for us
      //Try to clone --bare it first,
      val cloneResult = s"git clone --mirror $remoteUrl $localPath".!

      logger.info(s"CLONE RESULT: ${cloneResult}")

      if(cloneResult == 128) {
        //This indicates that we already had a cloned repo in there
        logger.info("Repo already cloned, fetching")
        Process(Seq("git", "fetch", "-q"), new File(localPath)).! match {
          case 0 => logger.info(s"Successful mirror of $remoteUrl to $localPath")
          case x => logger.error(s"Something went wrong! Exit code: $x")
        }
      }
    }
  }
}
