package is.kow.finatratypesafe

import com.twitter.finatra.http.HttpServer
import com.twitter.finatra.http.filters.CommonFilters
import com.twitter.finatra.http.routing.HttpRouter

object MirrorerServerMain extends MirrorerServer

class MirrorerServer extends HttpServer {
  val repoBase = flag("repo.base", "/srv/gitMirror", "Base for mirroing git repos")

  override def configureHttp(router: HttpRouter): Unit = {
    router
      .filter[CommonFilters]
      .add[MirroringHookController]

    log.info("This is a log message")
  }
}
