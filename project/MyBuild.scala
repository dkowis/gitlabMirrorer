import sbt._
import Keys._
import sbtassembly.AssemblyPlugin.autoImport._

object MyBuild extends Build {

  val finatraVersion = "2.0.1"

  lazy val root = Project(
    "gitlabMirrorer",
    file(".")).settings(
      name := "gitlabMirrorer",
      organization := "is.kow",
      version := "1.0-SNAPSHOT",
      scalaVersion := "2.11.7",
      scalacOptions := Seq(
        "-encoding", "UTF-8", "-deprecation", "-feature", "-unchecked",
        "-Ywarn-dead-code", "-Ywarn-numeric-widen", "-Ywarn-unused-import",
        "-language:existentials", "-language:higherKinds", "-language:implicitConversions"),
      ivyScala := ivyScala.value map {
        _.copy(overrideScalaVersion = true)
      },
      fork in test := true,
      assemblyMergeStrategy in assembly := {
        case "BUILD" => MergeStrategy.discard
        case other => MergeStrategy.defaultMergeStrategy(other)
      },
      resolvers ++= Seq(
        "Twitter maven" at "http://maven.twttr.com"
      ),
      libraryDependencies ++= Seq(
        "com.twitter.finatra" %% "finatra-http" % finatraVersion,
        "com.twitter.finatra" %% "finatra-slf4j" % finatraVersion,
        "ch.qos.logback" % "logback-classic" % "1.1.3", //Logging

        "com.typesafe.play" %% "play-json" % "2.4.2", //For awesome JSON parsing!

        //Test dependencies for finatra
        "com.twitter.finatra" %% "finatra-http" % finatraVersion % "test",
        "com.twitter.inject" %% "inject-server" % finatraVersion % "test",
        "com.twitter.inject" %% "inject-app" % finatraVersion % "test",
        "com.twitter.inject" %% "inject-core" % finatraVersion % "test",
        "com.twitter.inject" %% "inject-modules" % finatraVersion % "test",
        "com.twitter.finatra" %% "finatra-http" % finatraVersion % "test" classifier "tests",
        "com.twitter.finatra" %% "finatra-jackson" % finatraVersion % "test" classifier "tests",
        "com.twitter.inject" %% "inject-server" % finatraVersion % "test" classifier "tests",
        "com.twitter.inject" %% "inject-app" % finatraVersion % "test" classifier "tests",
        "com.twitter.inject" %% "inject-core" % finatraVersion % "test" classifier "tests",
        "com.twitter.inject" %% "inject-modules" % finatraVersion % "test" classifier "tests",

        "org.mockito" % "mockito-core" % "1.10.19" % "test",
        "org.scala-lang.modules" %% "scala-xml" % "1.0.3" % "test", //I want the xml support in test mode
        "org.scalatest" %% "scalatest" % "2.2.4" % "test", // testing
        "junit" % "junit" % "4.12" % "test"
      ).map(_.exclude("commons-logging", "commons-logging"))
    )
}

// vim: set ts=4 sw=4 et:
